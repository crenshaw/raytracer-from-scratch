# Buidling a raytracer from scratch
## SommerCampus TF - Uni Freiburg 2018

> *"The first approximation to a correct result is an error"*

![Final result](https://github.com/nikolausmayer/raytracing-from-scratch/raw/master/img/final-optimized.gif)  
(credits: Nikolaus Mayer)

### Prequisites
* C++ compiler: g++ >= 4.8
* Graphical UI
* Image viewer: e.g. display, eog, feh, geeqie

### Code reference
<https://github.com/nikolausmayer/raytracing-from-scratch>

> *"Die erste Annäherung an ein korrektes Ergebnis ist ein Fehler"*  