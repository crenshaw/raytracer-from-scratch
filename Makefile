
raytracer: main.cpp
	g++ -W -Wall -Wextra -Wpedantic -O3 -std=c++11 -o raytracer main.cpp

raytracer_debug:
	g++ -W -Wall -Wextra -Wpedantic -g -fsanitize=address -std=c++11 -o raytracer main.cpp
clean:
	rm raytracer