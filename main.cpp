
/****************************************************************************** 
 * 
 * a simple raytracer from scratch
 * 
 * 
******************************************************************************/

#include <cmath>
#include <cstdlib>
#include <fstream>
#include <iostream>
#include <vector>

int RESOLUTION_X = 1024;
int RESOLUTION_Y = RESOLUTION_X;

// sample uniform noise
float uniform_random() {
    // rand elem [-0.5, 0.5]
    return (float)(std::rand()) / RAND_MAX - 0.5f;
}

struct Vector {
    Vector(float x = 0.f, float y = 0.f, float z = 0.f) : x(x), y(y), z(z) {}
    // copy ctor
    Vector(const Vector& r) : x(r.x), y(r.y), z(r.z) {}

    float x, y, z;

    Vector operator+(const Vector& r) const {
        return Vector(x + r.x, y + r.y, z + r.z);
    }
    Vector operator-(const Vector& r) const {
        return *this + (r * -1);
    }
    // negate vector -> Vektor umdrehen
    Vector operator-() const {
        return *this * -1;
    }
    Vector operator*(float v) const {
        return Vector{x * v, y * v, z * v};
    }
    // dot product
    float operator%(const Vector& r) const {
        return (x * r.x) + (y * r.y) + (z * r.z);
    }
    // other product
    Vector operator^(const Vector& r) const {
        return Vector{
            (y * r.z) - (z * r.y),
            (z * r.x) - (x * r.z),
            (x * r.y) - (y * r.x)};
    }
    float length() const {
        return std::sqrt(*this % *this );
    }
    // normalize the vector
    Vector operator!() const {
        return (*this) * (1. / length());
    }

};

/* abstract class for objects in the scene */
struct Object {
    Object() : color{255, 255, 255}, reflectivity(1) {}

    virtual bool is_hit_by_ray(const Vector& incoming_ray_origin,
        const Vector& incoming_ray_direction, Vector& outgoing_ray_origin, Vector& outgoing_ray_direction,
        float& distance, Vector& hit_color, float& object_reflectivity) const = 0;
    

    void set_color(const Vector& v) {
        color = v;
    }

    void set_reflectivity(float v) {
        reflectivity = v;
    }

    Vector color;
    float reflectivity;
};

struct Triangle : Object {
    Triangle(const Vector& in_p0, const Vector& in_p1, const Vector& in_p2) : p0(in_p0), p1(in_p1), p2(in_p2) {
        u = p1 - p0;
        v = p2 - p0;
        normal = (v ^ u);
    }

    Vector p0, p1, p2;
    Vector u, v, normal;

    bool is_hit_by_ray(const Vector& incoming_ray_origin, const Vector& incoming_ray_direction, 
        Vector& outgoing_ray_origin, Vector& outgoing_ray_direction, float& distance, Vector& hit_color, float& object_reflectivity) const {
        
        // check ray direction
        if (normal % incoming_ray_direction > 0) {
            return false;
        }
        const float& pox{p0.x};
        const float& poy{p0.y};
        const float& poz{p0.z};
        const float& ux{u.x};
        const float& uy{u.y};
        const float& uz{u.z};
        const float& vx{v.x};
        const float& vy{v.y};
        const float& vz{v.z};
        const float& rx{incoming_ray_direction.x};
        const float& ry{incoming_ray_direction.y};
        const float& rz{incoming_ray_direction.z};
        const float& ox{incoming_ray_origin.x};
        const float& oy{incoming_ray_origin.y};
        const float& oz{incoming_ray_origin.z};
        const float u_factor = (-(ox - pox)*(ry*vz - rz*vy) + (oy - poy)*(rx*vz - rz*vx) \
            - (oz - poz)*(rx*vy - ry*vx))/(
                rx*uy*vz - rx*uz*vy - ry*ux*vz + ry*uz*vx + rz*ux*vy - rz*uy*vx);
        const float v_factor = ((ox - pox)*(ry*uz - rz*uy) - (oy - poy)*(rx*uz - rz*ux) \
            + (oz - poz)*(rx*uy - ry*ux))/(
                rx*uy*vz - rx*uz*vy - ry*ux*vz + ry*uz*vx + rz*ux*vy - rz*uy*vx);
        const float ray_factor = (-(ox - pox)*(uy*vz - uz*vy) + (oy - poy)*(ux*vz - uz*vx) \
            - (oz - poz)*(ux*vy - uy*vx))/(
                rx*uy*vz - rx*uz*vy - ry*ux*vz + ry*uz*vx + rz*ux*vy - rz*uy*vx);

        // test conditions of intersection test
        if (u_factor < 0 || v_factor < 0 || u_factor + v_factor > 1 || ray_factor < 0) {
            return false;
        }
        // prevent same object to be hit again directly caused by floating point unaccuracies
        if (ray_factor < 1e-6) {
            return false;
        }
        outgoing_ray_origin = p0 + (u * u_factor) + (v * v_factor);
        // tmp = n * (n * -R) -> reflects the incoming ray up like a mirror
        const Vector tmp{!normal * (!normal % -incoming_ray_direction)};
        outgoing_ray_direction = !(incoming_ray_direction + tmp * 2);  // important: normalize to 1

        distance = ray_factor;
        hit_color = color;
        object_reflectivity = this->reflectivity;
        return true;
    }
};



/* right now renders checkerboard */
Vector get_ground_color(const Vector& ray_origin, const Vector& ray_direction) {
    float distance = -ray_origin.y / ray_direction.y;
    float X = ray_origin.x + ray_direction.x * distance;
    float Z = ray_origin.z + ray_direction.z * distance;

    // import texture
    static unsigned char* texture_data{nullptr};
    const int tex_w{600};
    const int tex_h{400};

    if (not texture_data) {
        std::ifstream texture("texture.ppm");
        texture_data = new unsigned char[tex_w * tex_h * 3];
        texture.read(reinterpret_cast<char*>(texture_data), 15);
        texture.read(reinterpret_cast<char*>(texture_data), tex_w * tex_h * 3);  // overwrite header data
    }
    const int tex_x = std::abs((int)(X*100)) % tex_w;
    const int tex_y = std::abs((int)(Z*100)) % tex_h;
    
    const size_t pixel_index = (tex_y * tex_w + tex_x) * 3;

    return Vector(texture_data[ pixel_index + 0],
                                texture_data[pixel_index + 1],
                                texture_data[pixel_index + 2]);

    // if (((int)std::abs(std::floor(X)) % 2) == ((int)std::abs(std::floor(Z)) % 2)) {
    //     return Vector{255, 0, 0};
    // } else {
    //     return Vector{255, 255, 255};
    // }
}
/* return a fading gradient from black to some dark blue, from top to the horizon */
Vector get_sky_color(const Vector& ray_direction) {
    return Vector{0.7, 0.6, 1} * 255 * std::pow(1 - ray_direction.y, 2);
}


int main() {
    // define the coordinate system base. x:left, y:down, z:front
    float zoom_factor = 0.002;
    const Vector RIGHT{zoom_factor, 0, 0};
    const Vector UP{0, zoom_factor, 0};
    const Vector AHEAD{0, 0, 1};

    std::vector<Object*> scene_objects;

    // create some triangles; define counterclockwise from camera view
    scene_objects.push_back(new Triangle{Vector{-2, 0, 1}, Vector{2, 0, 1}, Vector{0, 3, 0.9}});
    scene_objects.back()->set_color(Vector{0, 0, 255});
    scene_objects.back()->set_reflectivity(0.8);

    scene_objects.push_back(new Triangle{Vector{2, 0, -5}, Vector{-2, 0, -5}, Vector{0, 3, -4.9}});
    scene_objects.back()->set_color(Vector{255, 255, 255});
    scene_objects.back()->set_reflectivity(0.8);

    // scene_objects.push_back(new Triangle{Vector{-0.25, 0.75, -1}, Vector{0.75, 0.75, -1}, Vector{0.25, 2, -1}});
    // scene_objects.back()->set_color(Vector{255, 0, 0});

    // create file and open write stream
    std::ofstream outfile("img.ppm");

    // define the header
    outfile << "P3 " << std::to_string(RESOLUTION_X) << " " << std::to_string(RESOLUTION_Y) << " 255";
    int X_lower = ((-1) * (RESOLUTION_X / 2)) + 1;
    int X_upper = RESOLUTION_X / 2;
    int Y_upper = RESOLUTION_Y / 2;
    int Y_lower = ((-1) * (RESOLUTION_Y / 2)) + 1;
    // write each pixel
    for (int y = Y_upper; y >= Y_lower; --y) {
        for (int x = X_lower; x <= X_upper; ++x) {

            Vector antialiased_color;
            const int pixel_samples{64};  // rendering scales linearly with this

            // anti-aliasing loop
            for (int sample = 0; sample < pixel_samples; ++sample) {
                // use Vector class as color storage also
                Vector final_color;  // default ctor initializes to zero
                Vector color;
                float ray_energy_left{1};
                // the camera position
                Vector ray_origin = Vector{0, 1, -4};
                // compute the ray direction for each pixel in the image
                // apply randomness in [-0.5, 0.5]: ray still through pixel
                Vector ray_direction = !Vector{RIGHT * (x - 0.5 + uniform_random()) +
                                                  UP * (y - 0.5 + uniform_random()) + 
                                                  AHEAD};

                Vector ray_hit_at;
                Vector ray_bounced_direction;

                const int max_hit_bounces{100};  // max no of reflections per ray
                for (int bounce = 0; bounce < max_hit_bounces; ++bounce) {


                    float distance;
                    float min_distance{99999.f};
                    float reflectivity_at_hit;
                    bool object_was_hit{false};
                    Object* hit_object{nullptr};
                    
                    // check all scene objects if hit
                    for (const auto& object : scene_objects) {
                        if (object->is_hit_by_ray(ray_origin, ray_direction, 
                                ray_hit_at, ray_bounced_direction, distance, color, reflectivity_at_hit)) {
                            object_was_hit = true;
                            // perform depth test here
                            if (distance < min_distance) {
                                hit_object = object;
                                min_distance = distance;
                            }
                        }
                    }
                    if (object_was_hit) {
                        // perform intersection test again to get actual color of nearest object
                        hit_object->is_hit_by_ray(ray_origin, ray_direction, 
                            ray_hit_at, ray_bounced_direction, distance, color, reflectivity_at_hit);

                            // for next iteration of bounce loop, 
                            // update ray origin and direction to reflected ray
                            ray_origin = ray_hit_at;
                            ray_direction = ray_bounced_direction;

                    } else {
                        // if pointing downwards, e.g. it reaches the earth somewhere
                        if (ray_direction.y < 0) {
                            color = get_ground_color(ray_origin, ray_direction);
                            reflectivity_at_hit = 0;
                            // break;  // make ground noreflect
                        } else {
                            color = get_sky_color(ray_direction);
                            reflectivity_at_hit = 0;
                            // break;  // make sky noreflect
                        }
                    }
                    // mix color after every bounce
                    final_color = final_color + (color * (ray_energy_left * (1 - reflectivity_at_hit)));
                    ray_energy_left = ray_energy_left * reflectivity_at_hit;
                    if (ray_energy_left <= 0) {
                        // stop ray
                        break;
                    }
                }
                antialiased_color = antialiased_color + final_color;
            }
            antialiased_color = antialiased_color * (1.0 / pixel_samples);
            
            // bound color to {0, 255} and convert to integer: (ganze Zahl)
            outfile << " " << (int)(std::max(0.f, std::min(antialiased_color.x, 255.f)))
                    << " " << (int)(std::max(0.f, std::min(antialiased_color.y, 255.f)))
                    << " " << (int)(std::max(0.f, std::min(antialiased_color.z, 255.f)));
        }
    }
    outfile << "\n";  // 'display' tool expects newline character
    outfile.close();

    return EXIT_SUCCESS;
}